package plotterprogram;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FunctionPlotter {
    private int imageWidth;
    private int imageHeight;
    private BufferedImage image;
    private Graphics2D g;

    public FunctionPlotter(int imageWidth, int imageHeight) {
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        this.g = this.image.createGraphics();
    }

    public void prepare() {
        this.g.setColor(Color.white);
        this.g.fillRect(0, 0, this.imageWidth, this.imageHeight);
        
        setAllLines();
        //this.g.setColor(Color.gray);
        //this.g.drawString("Hello World!", 0, 50);  //for strings
    }

    public void plot(IFunction function) {
        float domainFrom = -this.imageWidth / 2;
        float domainTo = this.imageWidth / 2;

        int lastU = 0;
        int lastV = 0;

        boolean firstPoint = true;

        this.g.setColor(Color.blue);

        for(float x = domainFrom; x <= domainTo; x+=1) {
            float y = function.f(x);

            int u = (int)x + 250;
            int v = 500 - ((int)y + 250);

            if (!firstPoint) {
                if (Float.isNaN(y)) {
                    lastV--;
                    this.g.drawOval(u - 3, lastV - 3, 6, 6);
                    v = lastV;
                } else {
                    this.g.drawLine(lastU, lastV, u, v);
                }
            } else {
                firstPoint = false;
            }

            lastU = u;
            lastV = v;
        }
    }

    public void save(String pngPath) throws IOException {
        File file = new File(pngPath);
        ImageIO.write(this.image, "png", file);
    }

    /**
     * Seting all grey and red lines
     */
    private void setAllLines() {

        for (int i = 0; i <= 500; i += 50) {
            if (i == 250) {
                this.g.setColor(Color.red);
            } else {
                this.g.setColor(Color.gray);
            }
            this.g.drawLine(i, 0, i, 500);
            this.g.drawLine(0, i, 500, i);
        }
    }
}
