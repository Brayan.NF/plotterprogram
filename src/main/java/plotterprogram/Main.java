package plotterprogram;

import java.io.IOException;

//import org.fundacionjala.IFunction;

public class Main {

    public static void main(String[] args) throws IOException {
        int imageWidth = 500;
        int imageHeight = 500;
        String pngPath = "my-function.png";

        FunctionPlotter plotter = new FunctionPlotter(imageWidth, imageHeight);
        plotter.prepare();
        IFunction funcion = new FuncionA();
        plotter.plot(funcion);
        plotter.save(pngPath);

        FunctionPlotter plotter3 = new FunctionPlotter(imageWidth, imageHeight);
        plotter3.prepare();
        IFunction funcionC = new FuncionC();
        plotter3.plot(funcionC);
        pngPath = "my-function-C.png";
        plotter3.save(pngPath);

        FunctionPlotter plotter4 = new FunctionPlotter(imageWidth, imageHeight);
        plotter4.prepare();
        IFunction funcionD = new FuncionD();
        plotter4.plot(funcionD);
        pngPath = "my-function-D.png";
        plotter4.save(pngPath);

        System.out.println("The image was generated at location: " + pngPath);
    }
}
