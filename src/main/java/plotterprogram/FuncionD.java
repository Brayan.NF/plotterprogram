package plotterprogram;

public class FuncionD implements IFunction {
    public float f(float x) {

        double result = (2000 / x) * Math.sin(x / 10);
        return (float) result;
    }
}