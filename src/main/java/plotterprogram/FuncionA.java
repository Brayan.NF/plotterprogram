package plotterprogram;

public class FuncionA implements IFunction {
    public float f(float x) {

        double result = (x * Math.sqrt(10))/(Math.sqrt(x + 10) - Math.sqrt(10));
        return (float) result;
    }
}