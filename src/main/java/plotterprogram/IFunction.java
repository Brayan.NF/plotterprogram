package plotterprogram;

public interface IFunction {
    float f(float x);
}
